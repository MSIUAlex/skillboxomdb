package ru.alexmsiu.appwithviewmodel.ui.main

import com.google.gson.annotations.SerializedName

data class MovieResponse(
    @SerializedName("Search")
    val movies: List<Movie>
)