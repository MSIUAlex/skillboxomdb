package ru.alexmsiu.appwithviewmodel.ui.main

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.util.AndroidRuntimeException
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainViewModel : ViewModel() {

    val movieListData : MutableLiveData<List<Movie>> = MutableLiveData()

    init {

           val retrofit =   Retrofit.Builder()
                   .baseUrl("http://omdbapi.com/")
                   .addConverterFactory(GsonConverterFactory.create(Gson()))
                   .build()
           val api =  retrofit.create(OmdbApi::class.java)
           viewModelScope.launch {
               val result = api.getAllMovies("Nicholas")
               result.body()?.movies?.let {
                   movieListData.postValue(it)
               }
           }


    }
}