package ru.alexmsiu.appwithviewmodel.ui.main

import android.R
import android.app.AlertDialog
import android.content.DialogInterface
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.main_fragment.*


class MovieAdapter(val data: List<Movie>) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    class ViewHolder(val item: Button): RecyclerView.ViewHolder(item)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val buttonView =  Button(parent.context)
        buttonView.width =  parent.width;
        return ViewHolder(buttonView)
    }
    override fun getItemCount(): Int = data.size
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.text = data[position].title

       holder.item.setOnClickListener{
           val builder = AlertDialog.Builder( holder.itemView.context)
           builder.setTitle("Информация о фильме: "+data[position].title)
           builder.setMessage("Год выхода: " + data[position].year +"\n" +
                              "Уникальный номер: " + data[position].imdbID +"\n" +
                              "Тип: " + data[position].type)
           builder.setPositiveButton("Ok"){dialog, which ->
               dialog.cancel()
           }
           val dialog: AlertDialog = builder.create()
           dialog.show()
       }
    }
}