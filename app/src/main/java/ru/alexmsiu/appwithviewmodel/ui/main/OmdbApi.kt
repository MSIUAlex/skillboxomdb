package ru.alexmsiu.appwithviewmodel.ui.main

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface OmdbApi {
    @GET("/")
    suspend fun getAllMovies(
        @Query("s")search: String,
        @Query("apiKey") apiKey: String = "d64c8123"
    ): Response<MovieResponse>
}