package ru.alexmsiu.appwithviewmodel.ui.main

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.main_fragment.*
import ru.alexmsiu.appwithviewmodel.R
import kotlin.system.exitProcess

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        if(isConnected){
            super.onActivityCreated(savedInstanceState)
            viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
            val recyclerList =  recycleView;
            recyclerList?.layoutManager =LinearLayoutManager(requireContext())
            viewModel.movieListData.observe(viewLifecycleOwner, Observer {
                recyclerList?.adapter = MovieAdapter(it)
            })
        }else{
            super.onActivityCreated(savedInstanceState)
            val builder = AlertDialog.Builder(context)
            builder.setTitle("Внимание!")
            builder.setMessage("Проверьте интернет соединение на вашем устройстве!")
            builder.setPositiveButton("Ok"){dialog, which ->
                startActivity(Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
                exitProcess(-1)

            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }



}